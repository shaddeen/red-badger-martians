# red-badger-martians

Here you'll find the source-code for a node script to help the many Red Badger martians queueing up to roam planet Mars find their way. It won't be easy, and we may lose some along the way; but each martian understands the importance of rolling around in the rough terrain in order to keep their coats that nice a shade of martian red, so they are prepared to make the ultimate sacrifice!

To begin their journey simply setup their martian environment:

```
npm i
```

And begin time:

```
npm start
```

First you'll need to enter the coordinates of the martian surface (e.g. 10 by 10):

```
10 10
```

Now we're all setup you can start dispatching your robots!

Begin by entering their initial position:

```
2 3 S
```

And then your sequence of instructions:

```
FFRLFFFLLRLF
```

Older generations will pass on their knowledge to newer ones and soon Mars will be a safe place to roam for every Red Badger martian robot.
