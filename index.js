#!/usr/bin/env node

import readline from "readline";
import { processInput } from "./src";

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

rl.prompt();

rl.on("line", input => {
  processInput(input);
});

rl.on("close", input => {
  process.exit(0);
});
