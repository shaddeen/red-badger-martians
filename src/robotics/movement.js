export const rotate = (currentOrientation, direction) => {
  const orientations = {
    L: { N: "W", E: "N", S: "E", W: "S" },
    R: { N: "E", E: "S", S: "W", W: "N" }
  };

  return orientations[direction][currentOrientation];
};

export const moveForward = (initialPosition, offset) => {
  const { orientation } = initialPosition;
  const offsets = {
    x: { N: 0, E: 1, S: 0, W: -1 },
    y: { N: 1, E: 0, S: -1, W: 0 }
  };

  return {
    ...move(initialPosition, {
      x: offsets.x[orientation],
      y: offsets.y[orientation]
    }),
    orientation
  };
};

const move = (initialPosition, { x = 0, y = 0 }) => ({
  x: initialPosition.x + x,
  y: initialPosition.y + y
});
