import { rotate, moveForward } from "./movement";
import { checkForBadScent, addScent } from "../scent";

let terrain = {};

export const dispatchRobot = (robot, marsTerrain) => {
  terrain = marsTerrain;

  let currentPosition = robot.position;
  try {
    robot.instructions.forEach(instruction => {
      currentPosition = instruct(currentPosition, instruction);
    });

    log(currentPosition, false);
  } catch (_) {
    log(currentPosition, true);
  }
};

const log = (position, lost) =>
  console.log(
    `\n${position.x} ${position.y} ${position.orientation} ${
      lost ? "LOST" : ""
    }\n`
  );

const isWithinBounds = (position, terrain) =>
  position.x <= terrain.x &&
  position.x >= 0 &&
  position.y <= terrain.y &&
  position.y >= 0;

const instruct = (position, instruction) => {
  switch (instruction) {
    case "L":
    case "R":
      return {
        ...position,
        orientation: rotate(position.orientation, instruction)
      };

    case "F":
      const newPosition = moveForward(position, 1);

      if (isWithinBounds(newPosition, terrain)) {
        return newPosition;
      } else {
        if (!checkForBadScent(position, instruction)) {
          addScent(position, instruction);
          throw "Lost";
        }
      }

      return position;
    default:
      return position;
  }
};
