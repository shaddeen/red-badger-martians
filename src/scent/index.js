const scent = [];

export const checkForBadScent = (position, instruction) =>
  scent.some(
    item =>
      position.x === item.x &&
      position.y === item.y &&
      instruction === item.instruction
  );

export const addScent = (position, instruction) => {
  scent.push({ ...position, instruction });
};
