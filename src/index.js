import { dispatchRobot } from "./robotics";

let terrain;
const MAX_COORDINATE = 50;

const setupTerrain = coordinates => {
  const [x, y] = coordinates;
  if (x > 0 && x <= MAX_COORDINATE && y > 0 && y <= MAX_COORDINATE) {
    terrain = { x: Number(x), y: Number(y) };
  } else {
    console.warn("Invalid coordinates. Please make sure it is > 0 and <= 50");
  }
};

let robot = {};

export const processInput = input => {
  if (!terrain) {
    setupTerrain(input.split(" "));
  } else if (!robot.position) {
    const [x, y, orientation] = input.split(" ");
    robot.position = { x: Number(x), y: Number(y), orientation };
  } else if (!robot.instructions) {
    robot.instructions = [...input.substring(0, 99)];
    dispatchRobot(robot, terrain);
    robot = {};
  }
};
